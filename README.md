# Gitlab Wiki Template powered by Arc42

The arc42 Software Architecture Documentation Template for Gitlabs project wikis.

## Getting started

Create a wiki for your Gitlab project.

Clone the Wiki repository.

Copy the content of this repository to the root of the cloned wiki repository.

Edit and fill content as desired.

## arc42

arc42 is the documentation of our choice for writing software architecture for projects. This template is in large parts taken from the [arc42 Documentation](https://docs.arc42.org/home/).